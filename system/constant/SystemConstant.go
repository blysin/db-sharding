package constant

const (
	Date           = "2006-01-02"
	Time           = "15:04:05"
	Datetime       = "2006-01-02 15:04:05"
	DatetimeMill   = "2006-01-02 15:04:05.000"
	YyyyMMddHHmmss = "20060102150405"
	ParkRouteCache = "park_sharding_table"
)
