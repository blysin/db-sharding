package sqlparser

import (
	"fmt"
	"github.com/emirpasic/gods/lists/arraylist"
	"testing"
)

func testParse(t *testing.T, sql string) {
	stat, err := Parse(sql)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(stat)

	//lotCode := "2007"

	tables := arraylist.New()

	getTables(&stat, tables)

	fmt.Println(tables)
}

func getTables(stat *Statement, tables *arraylist.List) {
	switch v := (*stat).(type) {
	case *Select:
		rewriteSelect(v, tables)
	case *Insert:
	case *Update:
	case *Delete:
	case *Replace:
	case *Set:
	case *Begin:
	case *Commit:
	case *Rollback:
	case *SimpleSelect:
	case *Show:
	case *Admin:
	default:
	}
}

func rewriteSelect(stmt *Select, tables *arraylist.List) {
	fmt.Println("select")
	for _, expr := range stmt.From {
		rewriteTableExpr(&expr, tables)
	}

	for _, expr := range stmt.SelectExprs {
		rewriteSelectExpr(&expr, tables)
	}

}

func rewriteSelectExpr(expr *SelectExpr, tables *arraylist.List) {
	switch v := (*expr).(type) {
	case *NonStarExpr:
		rewriteNonStarExpr(v, tables)
	case *StarExpr:
		rewrite(v.TableName, tables)
	}
}

func rewriteNonStarExpr(expr *NonStarExpr, tables *arraylist.List) {
	rewriteRootExpr(&expr.Expr, tables)
}

func rewriteRootExpr(expr *Expr, tables *arraylist.List) {
	switch v := (*expr).(type) {
	case *OrExpr:
		rewriteExpressionExpr(&v.Right, tables)
		rewriteExpressionExpr(&v.Left, tables)
		break
	case *AndExpr:
		rewriteExpressionExpr(&v.Right, tables)
		rewriteExpressionExpr(&v.Left, tables)
		break
	case *Subquery:
		rewriteSelectStatment(&v.Select,tables)
	case *FuncExpr:
		for _, selectExpr := range v.Exprs {
			rewriteSelectExpr(&selectExpr,tables)
		}
	}

}

func rewriteSelectStatment(subQuery *SelectStatement, tables *arraylist.List) {
	switch v := (*subQuery).(type) {
	case *Select:
		rewriteSelect(v, tables)
	}
}

func rewriteTableExpr(expr *TableExpr, tables *arraylist.List) {
	//判断类型，如果是JoinTableExpr才做处理
	switch v := (*expr).(type) {
	case *JoinTableExpr:
		rewriteJoin(v, tables)
	case *AliasedTableExpr:
		rewriteAliased(v, tables)
	}

}

func rewriteAliased(expr *AliasedTableExpr, tables *arraylist.List) {
	switch v := (*expr).Expr.(type) {
	case *Subquery:
		fmt.Println("Subquery", v)
		break
	case *TableName:
		rewrite(v.Name, tables)
	}
}

func rewrite(name []byte, tables *arraylist.List) {
	tableName := string(name)
	tables.Add(tableName)
}

func rewriteExpressionExpr(expr *BoolExpr, tables *arraylist.List) {
	if expr == nil {
		return
	}
	//表达式类型，好像不用处理
	fmt.Println("表达式类型", expr)
}

func rewriteJoin(joinExpr *JoinTableExpr, tables *arraylist.List) {
	fmt.Println(joinExpr)
	if joinExpr.RightExpr != nil {
		rewriteTableExpr(&joinExpr.RightExpr, tables)
	}

	if joinExpr.LeftExpr != nil {
		rewriteTableExpr(&joinExpr.LeftExpr, tables)
	}

	if joinExpr.On != nil {
		rewriteExpressionExpr(&joinExpr.On, tables)
	}
}

func TestSimpleSelect(t *testing.T) {
	//sql := "select last_insert_id() as a"
	sql := "SELECT COUNT(DISTINCT c.id) AS count\nFROM t_card c\n         LEFT JOIN t_card_lot cl ON cl.cardId = c.id AND cl.deleted != 1\n         LEFT JOIN t_card_car cc ON cc.cardId = c.id AND cc.deleted != 1\nWHERE c.lotCodeAssist = '2007'\n  AND c.deleted != 1\n  AND exists((SELECT GROUP_CONCAT(IF(v.id IS NULL, IFNULL((SELECT IF(CURDATE() > vv.endTime, '0', '2')\n                                                           FROM t_card_validtime vv\n                                                           WHERE vv.lotId = l.id\n                                                             AND vv.deleted = 0\n                                                           ORDER BY vv.id DESC\n                                                           LIMIT 1), '0'), '1'))\n              FROM t_card_lot l\n                       LEFT JOIN t_card_validtime v ON l.id = v.lotId AND v.deleted = 0 AND\n                                                       CURDATE() BETWEEN v.beginTime AND v.endTime\n              WHERE l.cardId = c.id\n                AND l.deleted != 1))\n"
	fmt.Println(sql)
	testParse(t, sql)
}

//func rewriteSql(stmt *Select, sql string) {
//	lotCode := "2007"
//
//	stmt.Where.Expr
//
//}

func TestSet(t *testing.T) {
	sql := "set names gbk"
	testParse(t, sql)
}

func TestMixer(t *testing.T) {
	sql := `admin upnode("node1", "master", "127.0.0.1")`
	testParse(t, sql)

	sql = "show databases"
	testParse(t, sql)

	sql = "show tables from abc"
	testParse(t, sql)

	sql = "show tables from abc like a"
	testParse(t, sql)

	sql = "show tables from abc where a = 1"
	testParse(t, sql)

	sql = "show proxy abc"
	testParse(t, sql)
}
