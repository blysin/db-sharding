#程序路径
APP_HOME=$(pwd)
#程序ID文件
PID_FILE=${APP_HOME}/pid/db-sharding.pid

#判断文件是否存在，如果不存在则创建
if [ ! -f "$PID_FILE" ]; then
  　　touch "$PID_FILE"
  echo 0 >$PID_FILE
fi

run() {
  pid=$(cat $PID_FILE)

  if [ $pid = "0" ]; then
    echo "start process....."
    nohup ./db-sharding -config ./conf >>logs/db-sharding-console.log 2>&1 &
    echo $! >$PID_FILE &
    echo $! >$PID_FILE
  else
    epid=$(ps -ef | grep $pid | grep -v grep | awk '{print $2}')

    # echo $epid

    if [ ! -n "$epid" ]; then
      # echo "start process....."
      nohup ./db-sharding -config ./conf >>logs/db-sharding-console.log 2>&1 &
      echo $! >$PID_FILE &
      echo $! >$PID_FILE
    fi
  fi

}

while true; do
  run
  sleep 5
done
