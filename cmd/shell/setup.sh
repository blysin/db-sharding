#程序路径
APP_HOME=$(pwd)

#程序ID文件
PID_FILE=${APP_HOME}/pid/db-sharding.pid

DEAMON_PID_FILE=${APP_HOME}/pid/db-sharding-deamon.pid

#判断文件是否存在，如果不存在则创建
if [ ! -f "$PID_FILE" ]; then
   　　touch "$PID_FILE"
   echo 0 >$PID_FILE
fi

if [ ! -f "$DEAMON_PID_FILE" ]; then
   　　touch "$DEAMON_PID_FILE"
   echo 0 >$DEAMON_PID_FILE
fi

#输出
# echo "service name: $PROJECT_NAME"
# echo "app home: $APP_HOME"
# echo "java home: $JAVA_HOME"
# echo "class path: $CLASS_PATH"
# echo "main class: $MAIN_CLASS"
# echo "pid file : $PID_FILE"

pid=0

deamon_pid=0

#获得pid
getpid() {
   pid=$(cat $PID_FILE)
}

getdeamonpid() {
   deamon_pid=$(cat $DEAMON_PID_FILE)
}

#启动项目
dev() {
   getpid

   if [ $pid -ne 0 ]; then
      echo "================================"
      echo "warn:  already started! or bad close last time!"
      echo "================================"
   fi

   getdeamonpid
   if [ $deamon_pid -ne 0 ]; then
      echo "================================"
      echo "warn: deamon already started! or bad close last time!"
      echo "================================"
   fi


   echo -n "Starting..."
   # nohup $JAVA_HOME/bin/java -server $JAVA_OPTS -classpath $CLASS_PATH $MAIN_CLASS
   nohup ./db-sharding -config ./conf >>logs/db-sharding-console.log 2>&1 &
   echo $! >$PID_FILE
   getpid
   if [ $pid -ne 0 ]; then
      echo "================================="
      echo "[Start Success] (pid=$pid)"
      echo "================================"
   else
      echo "================================="
      echo "[Start Failed]"
      echo "================================="
   fi
}

start() {
   getdeamonpid
   if [ $deamon_pid -ne 0 ]; then
      echo "================================"
      echo "warn: deamon already started! or bad close last time!"
      echo "================================"
   fi

   echo "Starting..."
   # nohup $JAVA_HOME/bin/java -server $JAVA_OPTS -classpath $CLASS_PATH $MAIN_CLASS
   sh sharding-deamon.sh &
   echo $! >$DEAMON_PID_FILE
   getdeamonpid
   if [ $deamon_pid -ne 0 ]; then
      echo "================================="
      echo "[Start Success] (pid=$deamon_pid)"
      echo "================================"
   else
      echo "================================="
      echo "[Start Failed]"
      echo "================================="
   fi
}

#关闭项目
#使用 kill -15 pid 方式来关闭进程，使得程序可以执行shutdownhook释放资源
stop() {
   getdeamonpid
   if [ $deamon_pid -ne 0 ]; then
      echo "stoping pid $deamon_pid"
      kill -9 $deamon_pid
      echo "================================"
      echo "stoped"
      echo "================================"
   fi


   getpid

   if [ $pid -ne 0 ]; then
      echo "stoping pid $pid"
      kill -9 $pid
      echo "================================"
      echo "stoped"
      echo "================================"
   fi
}

restart() {
   stop
   start
}

##脚本入口
case "$1" in
'start')
   start
   ;;
'stop')
   stop
   ;;
'restart')
   restart
   ;;
'startdeamon')
   startdeamon
   ;;
*)
   echo "Usage: $0 {start|stop|restart|dev}"
   exit 1
   ;;
esac
exit 0
