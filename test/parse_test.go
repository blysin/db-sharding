package test

import (
	"db-sharding/sqlparser"
	"fmt"
	"regexp"
	"strings"
	"testing"
)

var sql = "/**lc=592011258*/SELECT\n\t\tid,\n\t\tdict_type as dictType,\n\t\tdict_name as mainTypeName,\n\t\t3 as itemType,\n\t\tdict_value as value,\n\t\tis_default as isDefault,\n\t\tdict_key as typeName,\n\t\tdict_key as text,\n\t\tdict_sort as sortNum,\n\t\tchangeable as itemTag,\n\t\tdict_status as isEnable\n\t\tFROM\n\t\tt_dict\n\t\tWHERE\n\t\tdict_status = 1\n\t\t \n\t\t\tand dict_type = 'CARD_TYPE'\n\t\t \n\t\t \n\t\t\tand lotCodeAssist='592011258'"

func TestRewrite(t *testing.T) {
	//sql := "/* ApplicationName=IntelliJ IDEA 2020.3.2 */ select /**lc=2007*/ * from t_dict where lotCodeAssist = 2007 "
	actual, lotCode := sqlparser.RewriteSelectStmt(sql)
	fmt.Println(lotCode)
	fmt.Println(actual)

}

func TestGetTableName(t *testing.T) {
	if sql[0:1] == "(" {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}

const lotCodeRegexpStr = "lotcodeassist\\s*=\\s*'?\\d+'?"

var lotCodeRegexp = regexp.MustCompile(lotCodeRegexpStr)

func TestReg(t *testing.T) {
	sql = "select count(1) from t_card_car where lotcodeassist = '1001' and cardid = 209 and deleted"
	res2 := lotCodeRegexp.FindAllString(sql, 1)
	fmt.Println(res2[0])

	var lotCode = res2[0]
	lotCode = lotCode[strings.Index(lotCode, "=")+1:]
	lotCode = strings.TrimSpace(strings.ReplaceAll(lotCode, "'", ""))
	fmt.Println(lotCode)

}
