/**lc=1001*/select s.id, s.specialdate, s.feetype, g.specialdate as isspecial, s.lotcodeassist as lotcode, s.remark
            from t_fee_specialdates s,
                 t_fee_date_group g
            where s.lotcodeassist = '1001'
              and g.cardtype = 3
              and s.feetype = g.id
              and s.isdelete = 0
              and s.specialdate >= '2021-02-01'
              and s.specialdate <= '2021-02-28'
