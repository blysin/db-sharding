package test

import (
	"db-sharding/config"
	"db-sharding/system/constant"
	"db-sharding/system/log4go"
	"fmt"
	"github.com/siddontang/go-yaml/yaml"
	"os"
	"strconv"
	"testing"
	"time"
)

func TestAddDatabase(t *testing.T) {
	//configFile, _ := ioutil.ReadFile("/data/workspaces/go/db-sharding/etc/mixer.conf.yaml")
	err := config.ParseConfigFile("/data/workspaces/go/db-sharding/etc/mixer.conf.yaml")
	if err != nil {
		fmt.Println(err)
	}

	for i := 4; i < 21; i++ {

		var dbName, host, user, pwd = "park" + strconv.Itoa(i), "127.0.0.1:23306", "nkc_test", "nkc_test_ZahJ5een"

		db := config.NodeConfig{
			Name:             dbName,
			DownAfterNoAlive: 240,
			IdleConns:        2,
			User:             user,
			Password:         pwd,
			DbName:           dbName,
			Master:           host,
		}

		nodes := *config.GlobalCfg.Nodes
		nodes = append(nodes, db)

		config.GlobalCfg.Nodes = &nodes
	}

	Write()

}

func Write() {
	fileDate := time.Now().Format(constant.YyyyMMddHHmmss)
	path := "/home/blysin/Desktop/config-sharding-" + fileDate + ".yaml"
	file, err := os.Create(path)
	if err != nil {
		log4go.Error("文件读取错误", err)
		return
	}
	defer file.Close()

	bytes, err := yaml.Marshal(config.GlobalCfg)
	if err != nil {
		log4go.Error(err)
	}

	_, err = file.Write(bytes)
	if err != nil {
		log4go.Error(err)
	}
}
