module db-sharding

go 1.14

require (
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548
	github.com/emirpasic/gods v1.12.0
	github.com/go-redis/redis/v8 v8.5.0
	github.com/pingcap/parser v0.0.0-20210107054750-53e33b4018fe
	github.com/pingcap/tidb v1.1.0-beta.0.20210205053311-631dbfdc3215
	github.com/pingcap/tipb v0.0.0-20210204051656-2870a0852037 // indirect
	github.com/siddontang/go-yaml v0.0.0-20140818030931-d686370b54f9
	github.com/tufanbarisyildirim/gonginx v0.0.0-20201220095337-c901064aa0d8
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	gopkg.in/yaml.v1 v1.0.0-20140924161607-9f9df34309c0 // indirect
)
