package proxy

import (
	"db-sharding/config"
	"db-sharding/nginx"
	"db-sharding/system/log4go"
	"fmt"
	"github.com/emirpasic/gods/maps/hashmap"
	"strconv"
	"strings"
	time2 "time"
)

var ProxyCache = *hashmap.New()

//检查代理状态，如果未创建代理，需要先创建，如果已经创建代理了，判断是否即将过期，如果是则需要线程去刷新链接
func CheckProxy(host string, lotCode string) error {
	//如果没有网络代理，直接返回
	if !config.GlobalCfg.Proxy.Enable {
		return nil
	}

	proxyPost, _ := strconv.Atoi(strings.Split(host, ":")[1])

	val, exist := ProxyCache.Get(proxyPost)
	if !exist {
		//同步创建代理
		ReloadConfig(proxyPost, lotCode)
	} else {
		switch v := val.(type) {
		case int64:
			if time2.Now().Unix() > v {
				//异步创建代理
				go ReloadConfig(proxyPost, lotCode)
			}
		}
	}
	return fmt.Errorf("创建代理失败车场[%s]，代理端口[%d]", lotCode, proxyPost)
}

func ReloadConfig(proxyPost int, lotCode string) {
	//如果没有网络代理，直接返回
	if !config.GlobalCfg.Proxy.Enable {
		return
	}
	log4go.Info("加载网络代理配置，车场：", lotCode, "，端口", proxyPost)
	host := GetProxyConfig(lotCode)
	log4go.Info("获取到的代理地址：", host, "，车场：", lotCode)
	nginx.ModifyProxyConfig(proxyPost, host)
	//4分钟
	time := time2.Now().Unix() + 240
	//time := time2.Now().Unix() + 24
	ProxyCache.Put(proxyPost, time)
}

//调用接口，获取代理配置
func GetProxyConfig(lotCode string) string {
	return "121.36.160.42:3506"
}
