package proxy

import (
	"db-sharding/client"
	"db-sharding/hack"
	. "db-sharding/mysql"
	"db-sharding/sqlparser"
	"db-sharding/system/log4go"
	"db-sharding/system/redis"
	"fmt"
	"github.com/pingcap/parser"
	"github.com/pingcap/parser/ast"
	_ "github.com/pingcap/tidb/types/parser_driver"
	"strconv"
	"strings"
	"sync"
)

func (c *Conn) handleQuery(sql string) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("execute %s error %v", sql, e)
			return
		}
	}()

	sql = strings.TrimRight(sql, ";")

	//走两个逻辑，1如果能解析出车场id，直接强制路由，如果不行，再走正常逻辑
	actual, lotCode := sqlparser.RewriteSelectStmt(sql)
	if len(lotCode) == 0 {
		return executeShard(sql, c)
	} else {
		return forceSharding(actual, lotCode, c)
	}

}

var SqlParse = parser.New()

func init() {

}

func forceSharding(actual string, lotCode string, c *Conn) error {
	//根据车场id获取分库
	//log4go.Info("强制路由：", *actual)
	co, err := getDatabaseSharding(lotCode, c, true)

	if err != nil {
		return err
	}

	result, err := co.Execute(actual)

	c.closeShardConn(co, err != nil)

	if err != nil {
		return err
	}

	return c.writeResultset(result.Status, result.Resultset)
}

func forceShardingExec(actual string, lotCode string, c *Conn, args []interface{}) error {
	//根据车场id获取分库
	log4go.Debug("强制路由：", actual)
	co, err := getDatabaseSharding(lotCode, c, false)

	if err != nil {
		return err
	}

	conns := []*client.SqlConn{co}

	var rs []*Result
	rs, err = c.executeInShard(conns, actual, args)
	//c.closeShardConns(conns, err != nil)

	if err == nil {
		err = c.mergeExecResult(rs)
	}
	c.closeShardConns(conns, err != nil)
	return err
}

//添加参数，isSelect
func getDatabaseSharding(lotCode string, c *Conn, isSelect bool) (*client.SqlConn, error) {
	if nodeName, err := redis.GetRoute(lotCode); err != nil {
		return nil, err
	} else {
		return getDataBase(c, nodeName, isSelect)
	}

}

func getDataBase(c *Conn, nodeName string, isSelect bool) (*client.SqlConn, error) {
	//获取分库链接
	node := c.server.getNode(nodeName)
	//判断代理是否已经创建
	CheckProxy(node.cfg.Master, node.cfg.ProxyLotCode)

	client, err := c.getConn(node, isSelect)
	if err != nil {
		log4go.Error(err.Error())
		return client, err
	}
	c.lastNodeName = nodeName

	//ping一下，如果不能用，则需要保持链接
	err = client.Ping()
	if err != nil {
		log4go.Error("链接不可用", err.Error())
		proxyPost, _ := strconv.Atoi(strings.Split(node.cfg.Master, ":")[1])
		ReloadConfig(proxyPost, node.cfg.ProxyLotCode)
	}

	return client, nil
}

func (c *Conn) getShardList(stmt sqlparser.Statement, bindVars map[string]interface{}) ([]*Node, error) {
	if c.schema == nil {
		return nil, NewDefaultError(ER_NO_DB_ERROR)
	}

	ns, err := sqlparser.GetStmtShardList(stmt, c.schema.rule, bindVars)
	if err != nil {
		log4go.Error(err.Error())
		return nil, err
	}

	if len(ns) == 0 {
		return nil, nil
	}

	n := make([]*Node, 0, len(ns))
	for _, name := range ns {
		n = append(n, c.server.getNode(name))
	}
	return n, nil
}

func (c *Conn) getConn(n *Node, isSelect bool) (co *client.SqlConn, err error) {
	if !c.needBeginTx() {
		if isSelect {
			co, err = n.getSelectConn()
		} else {
			co, err = n.getMasterConn()
		}
		if err != nil {
			log4go.Error(err.Error())
			return
		}
	} else {
		//获取链接，为什么会空指针
		var ok bool
		c.Lock()
		//这里获取事务的链接，但是被关闭了，说明事务管理那块有问题
		co, ok = c.txConns[n]
		c.Unlock()

		if !ok {
			if co, err = n.getMasterConn(); err != nil {
				return
			}

			if err = co.Begin(); err != nil {
				return
			}

			c.Lock()
			c.txConns[n] = co
			c.Unlock()
		}
	}

	//fmt.Println("指针：", &co.Conn)

	if err = co.UseDB(n.cfg.DbName); err != nil {
		return
	}

	if err = co.SetCharset(c.charset); err != nil {
		return
	}

	return
}

func (c *Conn) getShardConns(isSelect bool, stmt sqlparser.Statement, bindVars map[string]interface{}) ([]*client.SqlConn, error) {
	nodes, err := c.getShardList(stmt, bindVars)
	if err != nil {
		log4go.Error(err.Error())
		return nil, err
	} else if nodes == nil {
		return nil, nil
	}

	conns := make([]*client.SqlConn, 0, len(nodes))

	var co *client.SqlConn
	for _, n := range nodes {
		co, err = c.getConn(n, isSelect)
		if err != nil {
			log4go.Error(err.Error())
			break
		}

		conns = append(conns, co)
	}

	return conns, err
}

func (c *Conn) executeInShard(conns []*client.SqlConn, sql string, args []interface{}) ([]*Result, error) {
	var wg sync.WaitGroup
	wg.Add(len(conns))

	rs := make([]interface{}, len(conns))

	f := func(rs []interface{}, i int, co *client.SqlConn) {
		r, err := co.Execute(sql, args...)
		if err != nil {
			log4go.Error(err.Error())
			rs[i] = err
		} else {
			rs[i] = r
		}

		wg.Done()
	}

	for i, co := range conns {
		go f(rs, i, co)
	}

	wg.Wait()

	var err error
	r := make([]*Result, len(conns))
	for i, v := range rs {
		if e, ok := v.(error); ok {
			err = e
			break
		}
		r[i] = rs[i].(*Result)
	}

	return r, err
}

func (c *Conn) executeInShardNew(co *client.SqlConn, sql string, args []interface{}) ([]*Result, error) {
	var wg sync.WaitGroup
	wg.Add(1)

	rs := make([]interface{}, 1)

	f := func(rs []interface{}, i int, co *client.SqlConn) {
		r, err := co.Execute(sql, args...)
		if err != nil {
			log4go.Error(err.Error())
			rs[i] = err
		} else {
			rs[i] = r
		}

		wg.Done()
	}

	go f(rs, 0, co)

	wg.Wait()

	var err error
	r := make([]*Result, 1)
	for i, v := range rs {
		if e, ok := v.(error); ok {
			err = e
			break
		}
		r[i] = rs[i].(*Result)
	}

	return r, err
}

func (c *Conn) closeShardConns(conns []*client.SqlConn, rollback bool) {
	if c.isInTransaction() || !c.isAutoCommit() {
		return
	}

	for _, co := range conns {
		if rollback {
			co.Rollback()
		}

		co.Close()
	}
}

func (c *Conn) closeShardConn(co *client.SqlConn, rollback bool) {
	if c.isInTransaction() || !c.isAutoCommit() {
		return
	}

	if rollback {
		co.Rollback()
	}

	co.Close()
}

func (c *Conn) newEmptyResultset(stmt *sqlparser.Select) *Resultset {
	r := new(Resultset)
	r.Fields = make([]*Field, len(stmt.SelectExprs))

	for i, expr := range stmt.SelectExprs {
		r.Fields[i] = &Field{}
		switch e := expr.(type) {
		case *sqlparser.StarExpr:
			r.Fields[i].Name = []byte("*")
		case *sqlparser.NonStarExpr:
			if e.As != nil {
				r.Fields[i].Name = e.As
				r.Fields[i].OrgName = hack.Slice(nstring(e.Expr))
			} else {
				r.Fields[i].Name = hack.Slice(nstring(e.Expr))
			}
		default:
			r.Fields[i].Name = hack.Slice(nstring(e))
		}
	}

	r.Values = make([][]interface{}, 0)
	r.RowDatas = make([]RowData, 0)

	return r
}

func makeBindVars(args []interface{}) map[string]interface{} {
	bindVars := make(map[string]interface{}, len(args))

	for i, v := range args {
		bindVars[fmt.Sprintf("v%d", i+1)] = v
	}

	return bindVars
}

func (c *Conn) handleSelect(stmt *sqlparser.Select, sql string, args []interface{}) error {
	bindVars := makeBindVars(args)

	conns, err := c.getShardConns(true, stmt, bindVars)
	if err != nil {
		log4go.Error(err.Error())
		return err
	} else if conns == nil {
		r := c.newEmptyResultset(stmt)
		return c.writeResultset(c.status, r)
	}

	var rs []*Result

	//TODO 重写sql
	//rewriteSql(stmt, sql)

	rs, err = c.executeInShard(conns, sql, args)

	c.closeShardConns(conns, false)

	if err == nil {
		err = c.mergeSelectResult(rs, stmt)
	}

	return err
}

func (c *Conn) handleSelectNew(sql string) error {
	//先解析sql

	//走两个逻辑，1如果能解析出车场id，直接强制路由，如果不行，再走正常逻辑
	actual, lotCode := sqlparser.RewriteSelectStmt(sql)
	if len(lotCode) == 0 {
		//获取上次的链接库
		co, err := c.getLastConnectDb(true)
		if err != nil {
			log4go.Error(err.Error())
			return err
		}

		//defer co.Close()

		result, err := co.Execute(actual)
		c.closeShardConn(co, err != nil)
		if err != nil {
			log4go.Error(err.Error())
			return err
		}

		return c.writeResultset(result.Status, result.Resultset)
	} else {
		//强制路由
		return forceSharding(actual, lotCode, c)
	}
}

func (c *Conn) getLastConnectDb(isSelect bool) (*client.SqlConn, error) {
	nodeName := c.lastNodeName
	if len(nodeName) <= 0 {
		nodeName = "park1"
	}

	co, err := getDataBase(c, nodeName, isSelect)
	return co, err
}

func (c *Conn) beginShardConns(conns []*client.SqlConn) error {
	if c.isInTransaction() || !c.isAutoCommit() {
		return nil
	}

	for _, co := range conns {
		if err := co.Begin(); err != nil {
			return err
		}
	}

	return nil
}

func (c *Conn) beginShardConn(co *client.SqlConn) error {
	if c.isInTransaction() || !c.isAutoCommit() {
		return nil
	}

	if err := co.Begin(); err != nil {
		return err
	}

	return nil
}

func (c *Conn) commitShardConns(conns []*client.SqlConn) error {
	if c.isInTransaction() || !c.isAutoCommit() {
		return nil
	}

	for _, co := range conns {
		if err := co.Commit(); err != nil {
			return err
		}
	}

	return nil
}

func (c *Conn) commitShardConn(co *client.SqlConn) error {
	if c.isInTransaction() || !c.isAutoCommit() {
		return nil
	}

	if err := co.Commit(); err != nil {
		return err
	}

	return nil
}

func (c *Conn) handleExecInert(actual string, lotCode string, args []interface{}) error {
	if len(lotCode) == 0 {
		//获取上次的链接库
		co, err := c.getLastConnectDb(false)
		if err != nil {
			log4go.Error(err.Error())
			return err
		}

		if err = c.beginShardConn(co); err != nil {
			return err
		}
		result, err := co.Execute(actual)
		if err != nil {
			log4go.Error(err.Error())
			return err
		}
		err = c.commitShardConn(co)
		c.closeShardConn(co, err != nil)
		if err != nil {
			log4go.Error(err.Error())
			return err
		}

		return c.writeResultset(result.Status, result.Resultset)
	} else {
		//强制路由
		return forceShardingExec(actual, lotCode, c, args)
	}
}

func (c *Conn) handleExec(stmt sqlparser.Statement, sql string, args []interface{}) error {
	bindVars := makeBindVars(args)

	conns, err := c.getShardConns(false, stmt, bindVars)
	if err != nil {
		return err
	} else if conns == nil {
		return c.writeOK(nil)
	}

	var rs []*Result

	if len(conns) == 1 {
		rs, err = c.executeInShard(conns, sql, args)
	} else {
		//for multi nodes, 2PC simple, begin, exec, commit
		//if commit error, data maybe corrupt
		for {
			if err = c.beginShardConns(conns); err != nil {
				break
			}

			if rs, err = c.executeInShard(conns, sql, args); err != nil {
				break
			}

			err = c.commitShardConns(conns)
			break
		}
	}

	c.closeShardConns(conns, err != nil)

	if err == nil {
		err = c.mergeExecResult(rs)
	}

	return err
}

func (c *Conn) mergeExecResult(rs []*Result) error {
	r := new(Result)

	for _, v := range rs {
		r.Status |= v.Status
		r.AffectedRows += v.AffectedRows
		if r.InsertId == 0 {
			r.InsertId = v.InsertId
		} else if r.InsertId > v.InsertId {
			//last insert id is first gen id for multi row inserted
			//see http://dev.mysql.com/doc/refman/5.6/en/information-functions.html#function_last-insert-id
			r.InsertId = v.InsertId
		}
	}

	if r.InsertId > 0 {
		c.lastInsertId = int64(r.InsertId)
	}

	c.affectedRows = int64(r.AffectedRows)

	return c.writeOK(r)
}

func (c *Conn) mergeSelectResult(rs []*Result, stmt *sqlparser.Select) error {
	r := rs[0].Resultset

	status := c.status | rs[0].Status

	for i := 1; i < len(rs); i++ {
		status |= rs[i].Status

		//check fields equal

		for j := range rs[i].Values {
			r.Values = append(r.Values, rs[i].Values[j])
			r.RowDatas = append(r.RowDatas, rs[i].RowDatas[j])
		}
	}

	//to do order by, group by, limit offset
	c.sortSelectResult(r, stmt)
	//to do, add log here, sort may error because order by key not exist in resultset fields

	if err := c.limitSelectResult(r, stmt); err != nil {
		return err
	}

	return c.writeResultset(status, r)
}

func (c *Conn) mergeSelectResultNew(rs []*Result, stmt *ast.SelectStmt) error {
	r := rs[0].Resultset

	status := c.status | rs[0].Status

	//for i := 0; i < len(rs); i++ {
	//	status |= rs[i].Status
	//
	//	//check fields equal
	//
	//	for j := range rs[i].Values {
	//		r.Values = append(r.Values, rs[i].Values[j])
	//		r.RowDatas = append(r.RowDatas, rs[i].RowDatas[j])
	//	}
	//}
	//
	////to do order by, group by, limit offset
	//c.sortSelectResultNew(r, stmt)
	////to do, add log here, sort may error because order by key not exist in resultset fields
	//
	//if err := c.sortSelectResultNew(r, stmt); err != nil {
	//	return err
	//}

	return c.writeResultset(status, r)
}

func (c *Conn) sortSelectResultNew(r *Resultset, stmt *ast.SelectStmt) error {
	if stmt.OrderBy == nil {
		return nil
	}

	sk := make([]SortKey, len(stmt.OrderBy.Items))

	for i, o := range stmt.OrderBy.Items {
		switch v := o.Expr.(type) {
		case *ast.ColumnNameExpr:
			sk[i].Name = v.Name.OrigColName()
		}

		if o.Desc {
			sk[i].Direction = SortDesc
		} else {
			sk[i].Direction = SortAsc
		}

	}

	return r.Sort(sk)
}

func (c *Conn) sortSelectResult(r *Resultset, stmt *sqlparser.Select) error {
	if stmt.OrderBy == nil {
		return nil
	}

	sk := make([]SortKey, len(stmt.OrderBy))

	for i, o := range stmt.OrderBy {
		sk[i].Name = nstring(o.Expr)
		sk[i].Direction = o.Direction
	}

	return r.Sort(sk)
}

func (c *Conn) limitSelectResult(r *Resultset, stmt *sqlparser.Select) error {
	if stmt.Limit == nil {
		return nil
	}

	var offset, count int64
	var err error
	if stmt.Limit.Offset == nil {
		offset = 0
	} else {
		if o, ok := stmt.Limit.Offset.(sqlparser.NumVal); !ok {
			return fmt.Errorf("invalid select limit %s", nstring(stmt.Limit))
		} else {
			if offset, err = strconv.ParseInt(hack.String([]byte(o)), 10, 64); err != nil {
				return err
			}
		}
	}

	if o, ok := stmt.Limit.Rowcount.(sqlparser.NumVal); !ok {
		return fmt.Errorf("invalid limit %s", nstring(stmt.Limit))
	} else {
		if count, err = strconv.ParseInt(hack.String([]byte(o)), 10, 64); err != nil {
			return err
		} else if count < 0 {
			return fmt.Errorf("invalid limit %s", nstring(stmt.Limit))
		}
	}

	if offset+count > int64(len(r.Values)) {
		count = int64(len(r.Values)) - offset
	}

	r.Values = r.Values[offset : offset+count]
	r.RowDatas = r.RowDatas[offset : offset+count]

	return nil
}
